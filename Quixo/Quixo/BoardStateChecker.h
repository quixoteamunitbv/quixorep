#pragma once
#include "Board.h"
class BoardStateChecker
{
public:
	enum class State
	{
		None,
		Win
	};

public:
	static State Check(const Board& b, const Board::Position& position);
};


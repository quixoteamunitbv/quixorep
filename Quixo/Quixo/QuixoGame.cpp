
#include "QuixoGame.h"
#include "BoardStateChecker.h"
#include "../Logging/Logging.h"
#include <fstream>
#include <string>

QuixoGame::QuixoGame(std::reference_wrapper<Player> firstPlayer, std::reference_wrapper<Player> secondPlayer) :
	m_firstPlayer(firstPlayer), m_secondPlayer(secondPlayer)
{
	// empty
}

void QuixoGame::Run()
{
	std::ofstream of("log.log", std::ios::app);
	Logger log(of);
	log.log("Quixo Game started...", Logger::Level::Info);
	of.close();

	//main loop
	while (true)
	{
		viewBoard();
		pickingPiece();
		placingPiece();
		if (checkingBoardState() == true)
			return;
		swapPlayers();

		system("cls");
	}
}

void QuixoGame::pickingPiece()
{
	std::cout << "Now it's " << m_firstPlayer.get().GetName() << " turn.\n";

	Board::Position pickedPiece;
	while (true)
	{
		try
		{
			std::cout << "Enter the position of the piece you want to pick: (line + column)";
			pickedPiece = m_firstPlayer.get().PickPiece(std::cin, m_firstPlayer, m_board);
			break;
		}
		catch (const char* errorMessage)
		{
			std::cout << errorMessage << std::endl;
		}
	}
}

void QuixoGame::placingPiece()
{
	Board::Position placedPosition;
	while (true)
	{
		try
		{
			std::cout << "Now " << m_firstPlayer.get().GetName() << " is placing the picked piece.\n";
			placedPosition = m_firstPlayer.get().PlacePiece(m_firstPlayer, m_board);
			m_placedPosition = placedPosition;
			break;
		}
		catch (const char* errorMessage)
		{
			std::cout << errorMessage << std::endl;
		}
	}
}

bool QuixoGame::checkingBoardState()
{

	auto state = BoardStateChecker::Check(m_board, m_placedPosition);
	if (state == BoardStateChecker::State::Win)
	{
		std::cout << m_board << "\n";
		std::cout << "We have a winner!\nCongratulations: " << m_firstPlayer.get().GetName() << std::endl;
		return true;
	}
	return false;
}

void QuixoGame::viewBoard()
{
	std::cout << "The board looks like this:\n";
	std::cout << m_board << std::endl;
}

void QuixoGame::swapPlayers()
{
	std::ofstream of("log.log", std::ios::app);
	Logger log(of);
	log.log("Swap players...", Logger::Level::Info);
	of.close();

	std::swap(m_firstPlayer, m_secondPlayer);
}

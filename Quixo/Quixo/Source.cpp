#include "Player.h"
#include "QuixoGame.h"
#include "../Logging/Logging.h"
#include <iostream>
#include <fstream>

int main()
{
	std::ofstream of("log.log", std::ios::app);
	Logger log(of);
	log.log("Started app...", Logger::Level::Info);

	std::string player1name, player2name;
	std::string player1mark, player2mark;

	std::cout << "Enter the name of the first player: ";
	std::cin >> player1name;
	std::cout << "What do you choose? X/O: ";
	std::cin >> player1mark;
	Player player1(player1name, player1mark);

	std::cout << "Enter the name of the second player: ";
	std::cin >> player2name;
	if (player1mark == "X")
	{
		player2mark = "O";
		std::cout << player2name << ", you are playing with " << player2mark << "\n";
	}
	else
	{
		player2mark = "X";
		std::cout << player2name << ", you are playing with " << player2mark << "\n";
	}
	Player player2(player2name, player2mark);

	std::reference_wrapper<Player> firstPlayer = player1;
	std::reference_wrapper<Player> secondPlayer = player2;

	QuixoGame quixo(firstPlayer, secondPlayer);
	quixo.Run();

	of.close();
	system("pause");
}
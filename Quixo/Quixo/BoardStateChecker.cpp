#include "BoardStateChecker.h"
#include "../Logging/Logging.h"
#include <fstream>
#include <numeric>

auto reducer = [](const Piece& piece1, const std::optional<Piece> &piece2)
{
	return piece1 & piece2.value().GetCube();
};

template<class IteratorType>
BoardStateChecker::State CheckFiveOfAKind(IteratorType& first, IteratorType& last)
{

	Piece piece = std::accumulate(first, last, (first++)->value(), reducer);
	if (piece.GetCube() != Piece::Cube::Blank)
		return BoardStateChecker::State::Win;
	return BoardStateChecker::State::None;
}

BoardStateChecker::State BoardStateChecker::Check(const Board & b, const Board::Position & position)
{
	Board& board = const_cast<Board&>(b);
	const auto&[lineIndex, columnIndex] = position;

	auto[first, last] = board.GetLine(lineIndex);
	auto[firstCol, lastCol] = board.GetColumn(columnIndex);

	if (lineIndex == columnIndex)
	{
		auto[firstMainD, lastMainD] = board.GetMainDiagonal();
		if (CheckFiveOfAKind<Board::main_diagonal_iterator>(firstMainD, lastMainD) == State::Win)
			return State::Win;
	}
	else if (lineIndex == Board::kWidth - columnIndex - 1)
	{
		auto[firstSecD, lastSecD] = board.GetSecondaryDiagonal();
		if (CheckFiveOfAKind<Board::secondary_diagonal_iterator>(firstSecD, lastSecD) == State::Win)
			return State::Win;
	}

	if (CheckFiveOfAKind<Board::line_iterator>(first, last) == State::Win)
		return State::Win;
	if (CheckFiveOfAKind<Board::column_iterator>(firstCol, lastCol) == State::Win)
		return State::Win;

	return State::None;
}






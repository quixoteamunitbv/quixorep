#pragma once
#include"Board.h"
#include"Player.h"

class QuixoGame
{
public:
	QuixoGame(std::reference_wrapper<Player> firstPlayer, std::reference_wrapper<Player> secondPlayer);
	void Run();

private:
	void pickingPiece();
	void placingPiece();
	bool checkingBoardState();
	void viewBoard();
	void swapPlayers();

private:
	Board m_board;
	Piece m_piece;
	std::reference_wrapper<Player> m_firstPlayer;
	std::reference_wrapper<Player> m_secondPlayer;
	Board::Position m_placedPosition;
};
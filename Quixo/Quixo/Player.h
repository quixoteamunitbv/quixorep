#pragma once
#pragma once
#include<string>

#include "Board.h"

class Player
{
public:
	using PositionToCheck = std::pair<Board::Position, uint8_t>;

public:
	Player(const std::string& name, const std::string& mark);

	//Pick piece
	Board::Position verifyPickPiece(const uint16_t line, const uint16_t column, Board & board, Piece & piece) const;
	Board::Position PickPiece(std::istream& in, Player& player, Board& board) const;

	//Mutari
	Board::Position moveRight(const uint16_t line, const uint16_t column, Board & board, Piece & piece) const;
	Board::Position moveLeft(const uint16_t line, const uint16_t column, Board & board, Piece & piece) const;
	Board::Position moveDown(const uint16_t line, const uint16_t column, Board & board, Piece & piece) const;
	Board::Position moveUp(const uint16_t line, const uint16_t column, Board & board, Piece & piece) const;

	//Place piece
	Board::Position PlacePiece(Player &player, Board & board) const;
	friend std::ostream& operator << (std::ostream os, const Player& player);

	//Getteri si setteri
	std::string GetName() const;
	void SetMark(int mark);
	int GetMark() const;

private:
	std::string m_playerName;
	int m_playerMark;
};


#include "Player.h"
#include <string>
#include "../Logging/Logging.h"
#include <fstream>

uint16_t LINE = UINT16_MAX;
uint16_t COLUMN = UINT16_MAX;

Player::Player(const std::string& name, const std::string&mark)
{
	std::ofstream of("log.log", std::ios::app);
	Logger log(of);
	log.log("Player created...", Logger::Level::Info);

	m_playerName = name;
	if (mark == "X")
		m_playerMark = 1;

	if (mark == "O")
		m_playerMark = 0;
}

Board::Position Player::verifyPickPiece(const uint16_t line, const uint16_t column, Board& board, Piece &piece) const
{
	if (line <= 4 && line >= 0 && column >= 0 && column <= 4) 
	{
		Board::Position position = { static_cast<uint8_t>(line), static_cast<uint8_t>(column) };
		if ((line == 0 && (column == 0 || column == 1 || column == 2 || column == 3 || column == 4))
			|| (column == 4 && (line == 1 || line == 2 || line == 3 || line == 4))
			|| (line == 4 && (column == 0 || column == 1 || column == 2 || column == 3))
			|| (column == 0 && (line == 1 || line == 2 || line == 3)))
		{
			if (board[position].value().GetCube() == piece.GetCube() ||
				board[position].value().GetCube() == Piece::Cube::Blank)
				return position;

			throw "Cant't take your opponent piece!";
		}
		throw "Can't take that piece! Try another.";
	}
	throw "Please enter only two numbers from 0 to 4.";
}

Board::Position Player::PickPiece(std::istream & in, Player &player, Board& board) const
{
	std::ofstream of("log.log", std::ios::app);
	Logger log(of);
	log.log("Player picked a piece ...", Logger::Level::Info);

	std::cout << "(Now you pick a " << player.GetMark() << ")\n";
	Piece piece = (Piece::Cube) (m_playerMark + 1);

	if (in >> LINE)
		if (in >> COLUMN)
			return verifyPickPiece(LINE, COLUMN, board, piece);
}


Board::Position Player::moveRight(const uint16_t line, const uint16_t column, Board& board, Piece &piece) const
{
	Board::Position position = { static_cast<uint8_t>(line), 4 };
	auto& optionalPiece = board[position];

	if (column == 0 || (column != 0 && column != 4))
	{
		for (int columnS = column; columnS < 4; ++columnS)
			board[{line, columnS}] = std::move(board[{line, columnS + 1}]);
	}
	else throw "Can't move piece to the right. Choose another place to move the piece.";

	optionalPiece = std::move(piece);
	return position;
}

Board::Position Player::moveLeft(const uint16_t line, const uint16_t column, Board& board, Piece &piece) const
{
	Board::Position position = { static_cast<uint8_t>(line), 0 };
	auto& optionalPiece = board[position];

	if (column == 4 || (column != 0 && column != 4))
	{
		for (int columnS = column; columnS > 0; --columnS)
			board[{line, columnS}] = std::move(board[{line, columnS - 1}]);
	}
	else throw "Can't move piece to the left. Choose another place to move the piece.";

	optionalPiece = std::move(piece);
	return position;
}

Board::Position Player::moveDown(const uint16_t line, const uint16_t column, Board& board, Piece &piece) const
{
	Board::Position position = { 4, static_cast<uint8_t>(column) };
	auto& optionalPiece = board[position];

	if ((column == 0 || column == 4 || column != 0 || column != 4) && line != 4)
	{
		for (int lineS = line; lineS < 4; ++lineS)
			board[{lineS, column}] = std::move(board[{lineS + 1, column}]);
	}
	else throw "Can't move the piece down. Choose another place to move the piece.";

	optionalPiece = std::move(piece);
	return position;
}

Board::Position Player::moveUp(const uint16_t line, const uint16_t column, Board& board, Piece &piece) const
{
	Board::Position position = { 0, static_cast<uint8_t>(column) };
	auto& optionalPiece = board[position];

	if ((column == 0 || column == 4 || column != 0 || column != 4) && line != 0)
	{
		for (int lineS = line; lineS > 0; --lineS)
			board[{lineS, column}] = std::move(board[{lineS - 1, column}]);
	}
	else throw "Can't move the piece up. Choose another place to move the piece.";

	optionalPiece = std::move(piece);
	return position;
}

Board::Position Player::PlacePiece(Player &player, Board & board) const
{
	std::ofstream of("log.log", std::ios::app);
	Logger log(of);
	log.log("Player try to place a piece ...", Logger::Level::Info);

	Piece piece(static_cast<Piece::Cube>(player.GetMark() + 1));

	std::string position;
	std::cout << "Where do you want to move the piece? (Left/Right/Up/Down) ";
	std::cin >> position;

	log.log("Player placed a piece ...", Logger::Level::Info);

	if (position == "Right" || position == "right" || position == "RIGHT")
		return moveRight(LINE, COLUMN, board, piece);
	else
		if (position == "Left" || position == "left" || position == "LEFT")
			return moveLeft(LINE, COLUMN, board, piece);
		else
			if (position == "Down" || position == "down" || position == "DOWN")
				return moveDown(LINE, COLUMN, board, piece);
			else
				if (position == "Up" || position == "up" || position == "UP")
					return moveUp(LINE, COLUMN, board, piece);
	throw  "Insert position again.";
}

std::string Player::GetName() const
{
	return m_playerName;
}

void Player::SetMark(int mark)
{
	m_playerMark = mark;
}

int Player::GetMark() const
{
	return m_playerMark;
}

std::ostream& operator<<(std::ostream& os, const Player& player)
{
	return os << player.GetName();
}
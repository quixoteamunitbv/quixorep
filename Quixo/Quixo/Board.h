#pragma once
#include"../DLL/Piece.h"
#include <array>
#include<cstdint>
#include <optional>
class Board
{

public:
	using Position = std::pair<uint8_t, uint8_t>;

public:
	Board();

public:
	
	const std::optional<Piece>& operator[] (const Position& position) const;
	std::optional<Piece>& operator[] (const Position& pos);
	friend std::ostream& operator<<(std::ostream& outputStream, const Board& board);

public:
	static const size_t kWidth = 5;
	static const size_t kHeight = 5;
	static const size_t kSize = kWidth * kHeight;

public:
	std::array<std::optional<Piece>, kSize> m_pieces;
	
public:
	class base_iterator
	{
		const std::optional<Piece> kInvalidPiece;

	public:
		base_iterator(std::array<std::optional<Piece>, kSize>& data, size_t offset);
		const std::optional<Piece>& operator *();
		const std::optional<Piece>* operator ->();
		bool operator != (const base_iterator& other);

	protected:
		std::array<std::optional<Piece>, kSize>& m_data;
		size_t m_offset;
	};
public:
	class line_iterator : public base_iterator
	{
	public:
		using base_iterator::base_iterator;

		line_iterator& operator ++();
		line_iterator operator ++(int);
	};

public:
	class column_iterator : public base_iterator
	{
	public:
		using base_iterator::base_iterator;

		column_iterator& operator ++();
		column_iterator operator ++(int);
	};

	class main_diagonal_iterator : public base_iterator
	{
	public:
		using base_iterator::base_iterator;

		main_diagonal_iterator& operator ++();
		main_diagonal_iterator operator ++(int);
	};

	class secondary_diagonal_iterator : public base_iterator
	{
	public:
		using base_iterator::base_iterator;

		secondary_diagonal_iterator& operator ++();
		secondary_diagonal_iterator operator ++(int);
	};

public:
	std::pair<line_iterator, line_iterator> GetLine(uint32_t index);
	std::pair<column_iterator, column_iterator> GetColumn(uint32_t index);
	std::pair<main_diagonal_iterator, main_diagonal_iterator> GetMainDiagonal();
	std::pair<secondary_diagonal_iterator, secondary_diagonal_iterator> GetSecondaryDiagonal();
};
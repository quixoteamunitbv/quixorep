#pragma once
#include <iostream>
#include<cstdint>

#ifdef DLL_EXPORTS
#define PIECE_API __declspec(dllexport)
#else
#define PIECE_API __declspec(dllexport)
#endif 


class PIECE_API Piece
{

public:
	enum class Cube : uint8_t
	{
		Blank, // -1
		Circle, // 0
		Cross // 1
	};

public:
	Piece();
	Piece(Cube cube);
	Piece(const Piece &other);
	Piece(Piece && other);
	~Piece();

	Piece &operator = (const Piece &other);
	Piece & operator = (Piece && other);
	bool operator == (const Piece & rPiece) const;
	bool operator !=(const Piece & rPiece)  const;
	Piece& operator&=(const Piece & other);
	Piece operator&(Piece other) const;

	Piece::Cube GetCube() const;

	friend std::ostream& operator <<(std::ostream& os, const Piece& piece);

private:
	Cube m_cube : 2;
};


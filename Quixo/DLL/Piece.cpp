#include "Piece.h"
#include "../Logging/Logging.h"
#include <fstream>
#include <cassert>

Piece::Piece() :
	Piece(Cube::Blank)
{
	//Empty
}

Piece::Piece(Cube cube) :
	m_cube(cube)
{
	//Empty
	static_assert(sizeof(*this) <= 1, "This class should be 1 byte in size");
}

Piece::Piece(const Piece & other)
{
	*this = other;
}

Piece::Piece(Piece && other)
{
	*this = std::move(other);
}


Piece::~Piece()
{
	m_cube = Cube::Blank;
}

Piece & Piece::operator=(const Piece & other)
{
	m_cube = other.m_cube;

	return *this;
}

Piece & Piece::operator=(Piece && other)
{
	m_cube = other.m_cube;

	new(&other) Piece;

	return *this;
}

bool Piece::operator==(const Piece & rPiece) const
{
	return this->m_cube == rPiece.m_cube;
}

bool Piece::operator!=(const Piece & rPiece) const
{
	return this->m_cube != rPiece.m_cube;
}

Piece::Cube Piece::GetCube() const
{
	return m_cube;
}

std::ostream & operator<<(std::ostream & os, const Piece & piece)
{
	return os << static_cast<int>(piece.m_cube) - 1;
}
Piece& Piece::operator&=(const Piece & other)
{
	if (this->GetCube() != other.GetCube())
		this->m_cube = Piece::Cube::Blank;

	return *this;
}

Piece Piece::operator&(Piece other) const
{
	other &= *this;
	return other;
}
#include "stdafx.h"
#include "CppUnitTest.h"

#include "BoardStateChecker.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace QuixoTests
{
	TEST_CLASS(BoardStateCheckerTests)
	{
	public:


		TEST_METHOD(NoneOnePiece)
		{
			Board board;
			Board::Position lastPosition{ 2, 1 };
			board[lastPosition] = Piece(Piece::Cube::Circle);

			Assert::IsTrue(BoardStateChecker::Check(board, lastPosition) == BoardStateChecker::State::None);
		}

		TEST_METHOD(WinLineOne)
		{
			Board board;
			board[{0, 0}] = Piece(Piece::Cube::Circle);
			board[{0, 1}] = Piece(Piece::Cube::Circle);
			board[{0, 2}] = Piece(Piece::Cube::Circle);
			board[{0, 3}] = Piece(Piece::Cube::Circle);
			Board::Position lastPosition{ 0, 4 };
			board[lastPosition] = Piece(Piece::Cube::Circle);

			Assert::IsTrue(BoardStateChecker::Check(board, lastPosition) == BoardStateChecker::State::Win);
		}

		TEST_METHOD(WinColumnOne)
		{
			Board board;
			board[{0, 0}] = Piece(Piece::Cube::Circle);
			board[{1, 0}] = Piece(Piece::Cube::Circle);
			board[{2, 0}] = Piece(Piece::Cube::Circle);
			board[{3, 0}] = Piece(Piece::Cube::Circle);
			Board::Position lastPosition{ 4, 0 };
			board[lastPosition] = Piece(Piece::Cube::Circle);

			Assert::IsTrue(BoardStateChecker::Check(board, lastPosition) == BoardStateChecker::State::Win);
		}

		TEST_METHOD(WinMainDiagonal)
		{
			Board board;
			board[{0, 0}] = Piece(Piece::Cube::Circle);
			board[{1, 1}] = Piece(Piece::Cube::Circle);
			board[{2, 2}] = Piece(Piece::Cube::Circle);
			board[{3, 3}] = Piece(Piece::Cube::Circle);
			Board::Position lastPosition{ 4, 4 };
			board[lastPosition] = Piece(Piece::Cube::Circle);

			Assert::IsTrue(BoardStateChecker::Check(board, lastPosition) == BoardStateChecker::State::Win);
		}
		
		
		TEST_METHOD(WinSecondaryDiagonal)
		{
			Board board;
			board[{0, 4}] = Piece(Piece::Cube::Circle);
			board[{1, 3}] = Piece(Piece::Cube::Circle);
			board[{2, 2}] = Piece(Piece::Cube::Circle);
			board[{3, 1}] = Piece(Piece::Cube::Circle);
			Board::Position lastPosition{ 4, 0 };
			board[lastPosition] = Piece(Piece::Cube::Circle);

			Assert::IsTrue(BoardStateChecker::Check(board, lastPosition) == BoardStateChecker::State::Win);
		}
		TEST_METHOD(NotWinForDifferentPieces)
		{
			Board board;
			board[{0, 1}] = Piece::Cube::Cross;
			board[{1, 1}] = Piece::Cube::Cross;
			board[{2, 1}] = Piece::Cube::Cross;
			board[{3, 1}] = Piece::Cube::Cross;
			Board::Position lastPosition{ 4, 4 };
			board[lastPosition] = Piece(Piece::Cube::Circle);
			Assert::IsTrue(BoardStateChecker::Check(board, lastPosition) == BoardStateChecker::State::None);

		}

		TEST_METHOD(NotWinForAColumnThatIncludesABlankPiece)
		{
			Board board;
			board[{0, 1}] = Piece::Cube::Cross;
			board[{1, 1}] = Piece::Cube::Cross;
			board[{2, 1}] = Piece::Cube::Cross;
			board[{3, 1}] = Piece::Cube::Blank;

			Board::Position lastPosition = { 1,4 };
			board[lastPosition] = Piece::Cube::Cross;

			Assert::IsTrue(BoardStateChecker::Check(board, lastPosition) == BoardStateChecker::State::None);
		}

		TEST_METHOD(NotWinForALineThatIncludesABlankPiece)
		{
			Board board;
			board[{4, 0}] = Piece::Cube::Cross;
			board[{4, 1}] = Piece::Cube::Cross;
			board[{4, 2}] = Piece::Cube::Blank;
			board[{4, 3}] = Piece::Cube::Cross;

			Board::Position lastPosition = { 4,4 };
			board[lastPosition] = Piece::Cube::Cross;

			Assert::IsFalse(BoardStateChecker::Check(board, lastPosition) == BoardStateChecker::State::Win);

		}
	};
}
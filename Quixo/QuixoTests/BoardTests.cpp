#include "stdafx.h"
#include "CppUnitTest.h"
#include "Board.h"
//#include "Piece.h"
#include "../DLL/Piece.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace QuixoTests
{
	TEST_CLASS(BoardTests)
	{
	public:
		TEST_METHOD(ConstructorWithoutParameters)
		{
			Board board;
			Board::Position position;
			auto&[line, column] = position;
			for (; line < Board::kHeight; ++line)
			{
				for (column = 0; column < Board::kWidth; ++column)
				{
					if (board[position] != Piece::Cube::Blank)
						Assert::Fail();
				}
			}
		}

		TEST_METHOD(CheckBoardBracketOperatorSetAndGet)
		{
			Board board;
			Piece piece;
			Board::Position position{ 0, 0 };

			Assert::IsTrue(board[position].has_value());
			Assert::IsTrue(board[position]->GetCube() == piece.GetCube());
		}

		TEST_METHOD(CheckBoardBracketOperatorGetOutOfRangePositions)
		{
			Board board;
			Assert::ExpectException<std::out_of_range>(
				[&board]() {
				board[{Board::kHeight, 0}];
			});
			Assert::ExpectException<std::out_of_range>(
				[&board]() {
				board[{0, Board::kHeight}];
			});
		}


		TEST_METHOD(GetAtMinusOneZero)
		{
			Board board;

			Assert::ExpectException<std::out_of_range>(
				[&board]() {
				board[{-1, 0}];
			});
		}

		TEST_METHOD(GetAtMinusOneZeroConst)
		{
			const Board board;
			
			Assert::ExpectException<std::out_of_range>([&board]() {
				&board[{-1, 0}];
			});
		}

		TEST_METHOD(GetAtZeroMinusOne)
		{
			Board board;

			Assert::ExpectException<std::out_of_range>(
				[&board]() {
				board[{0, -1}];
			});
		}

		TEST_METHOD(GetAtZeroMinusOneConst)
		{
			const Board board;

			Assert::ExpectException<std::out_of_range>([&board]() {
				board[{0, -1}];
			});
		}
	};
}
#include "stdafx.h"
#include "CppUnitTest.h"
//#include "Piece.h"
#include "../DLL/Piece.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace QuixoTests
{
	TEST_CLASS(PieceTests)
	{
	public:

		TEST_METHOD(ConsructorWithoutParameters)
		{
			Piece piece;
			Assert::IsTrue(piece.GetCube() == Piece::Cube::Blank);
		}

		TEST_METHOD(ConsructorWithOneParameter)
		{
			Piece piece(Piece::Cube::Cross);
			Assert::IsTrue(piece.GetCube() == Piece::Cube::Cross);
		}

	};
}
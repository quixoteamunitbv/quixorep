#include "stdafx.h"
#include "CppUnitTest.h"
#include "Player.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace QuixoTests
{
	TEST_CLASS(PlayerTests)
	{
	public:

		TEST_METHOD(ConsructorWithTwoParameters)
		{
			std::string name = "Joe";
			std::string mark = "X";
			Player player(name, mark);

			Assert::IsTrue(player.GetName() == name && player.GetMark() == 1);
		}

		TEST_METHOD(PickPieceFromABlankSpace)
		{
			Board board;
			Piece piece(Piece::Cube::Circle);
			uint16_t line = 0;
			uint16_t column = 0;
			Board::Position position = { line, column };
			Player player("Player", "O");
		
			Assert::IsTrue(player.verifyPickPiece(line, column, board, piece) == position);
		}

		TEST_METHOD(PickPieceFromAnOccupiedPositionByTheSamePiece)
		{
			Board board;
			Piece piece(Piece::Cube::Circle);
			uint16_t line = 0;
			uint16_t column = 0;
			Board::Position position = { line, column };
			board[position] = piece;
			Player player("Player", "O");

			Assert::IsTrue(player.verifyPickPiece(line, column, board, piece) == position);
		}

		TEST_METHOD(TryToPickPieceFromAnOccupiedPositionByADifferentPiece)
		{
			Board board;
			Piece piece(Piece::Cube::Cross);
			uint16_t line = 0;
			uint16_t column = 0;
			Board::Position position = { line, column };
			board[position] = piece;
			Player player("Player", "O");

			try
			{
				player.verifyPickPiece(line, column, board, piece);
			}
			catch (const char* errorMessage)
			{
				Assert::Fail;
			}
		}

		TEST_METHOD(TryToPickAPieceOutOfTheConcentricSquare)
		{
			Board board;
			Piece piece(Piece::Cube::Circle);
			uint16_t line = 2;
			uint16_t column = 2;
			Board::Position position = { line, column };
			board[position] = piece;
			Player player("Player", "O");

			try
			{
				player.verifyPickPiece(line, column, board, piece);
			}
			catch (const char* errorMessage)
			{
				Assert::Fail;
			}
		}

		TEST_METHOD(TryMovePieceRight)
		{
			Board board;
			Piece piece(Piece::Cube::Circle);
			uint16_t line = 0;
			uint16_t column = 2;
			Board::Position posToMove = { line, 4 };
			board[posToMove] = piece;
			Player player("Player", "O");

			Assert::IsTrue(player.moveRight(line, column, board, piece) == posToMove);
		}

		TEST_METHOD(TryMovePieceLeft)
		{
			Board board;
			Piece piece(Piece::Cube::Circle);
			uint16_t line = 0;
			uint16_t column = 2;
			Board::Position posToMove = { line, 0 };
			board[posToMove] = piece;
			Player player("Player", "O");

			Assert::IsTrue(player.moveLeft(line, column, board, piece) == posToMove);
		}

		TEST_METHOD(TryMovePieceDown)
		{
			Board board;
			Piece piece(Piece::Cube::Circle);
			uint16_t line = 0;
			uint16_t column = 2;
			Board::Position posToMove = { 4, column };
			board[posToMove] = piece;
			Player player("Player", "O");

			Assert::IsTrue(player.moveDown(line, column, board, piece) == posToMove);
		}

		TEST_METHOD(TryMovePieceUp)
		{
			Board board;
			Piece piece(Piece::Cube::Circle);
			uint16_t line = 4;
			uint16_t column = 2;
			Board::Position posToMove = { 0, column };
			board[posToMove] = piece;
			Player player("Player", "O");

			Assert::IsTrue(player.moveUp(line, column, board, piece) == posToMove);
		}
	};
}